from inspect import signature


class ServiceContainer:
    def __init__(self):
        self.__beans = []
        self.__beans.append({"name": self.__get_qualified_name(ServiceContainer), "object": self})

    def get_bean(self, _class):
        class_path = self.__get_qualified_name(_class)
        for bean in self.__beans:
            if bean["name"] == class_path:
                return bean["object"]

        scope = self.__get_scope(_class)

        if not self.__check_scope(scope):
            raise Exception("Bad scope, scope name {}".format(scope))

        if scope == "prototype":
            bean = self.__prototype(_class)
        else:
            bean = self.__singleton(class_path, _class)

        return bean

    def bind(self, abstract_class, concrete_class):
        scope = self.__get_scope(concrete_class)

        if not self.__check_scope(scope):
            raise Exception("Bad scope, scope name {}".format(scope))

        self.__singleton(self.__get_qualified_name(abstract_class), concrete_class)

    def __singleton(self, name, _class):
        bean = self.__get_new_object(_class)
        self.__beans.append({"name": name, "object": bean})
        return bean

    def __prototype(self, _class):
        return self.__get_new_object(_class)

    def __get_new_object(self, _class):
        parameters = self.__get_parameters(_class)
        object_list = []
        for parameter in parameters:
            if parameter != "scope":
                object_list.append(self.get_bean(parameters[parameter].annotation))
        return _class(*object_list)

    def __get_qualified_name(self, _class):
        return _class.__module__ + "." + _class.__name__

    def __check_scope(self, scope) -> bool:
        available = ["singleton", "prototype"]

        if scope in available:
            return True
        else:
            return False

    def __get_scope(self, _class):
        parameters = self.__get_parameters(_class)
        for parameter in parameters:
            if parameters[parameter].name == "scope":
                return parameters[parameter].default

        return "singleton"

    def __get_parameters(self, _class):
        return signature(_class).parameters
