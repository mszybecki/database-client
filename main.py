import os
import sys
import atexit

from PySide2.QtWidgets import QApplication
from core import FileProvider, StandardFileProvider, ServiceContainer, Environment, Database
from ui import MainWindow

if __name__ == '__main__':
    container = ServiceContainer()
    container.bind(FileProvider, StandardFileProvider)

    environment = container.get_bean(Environment)
    environment.put("base_dir", os.getcwd())

    database = container.get_bean(Database)
    database.restore_tables()

    app = QApplication(sys.argv)
    window = container.get_bean(MainWindow)
    window.show()

    atexit.register(database.save_tables)
    sys.exit(app.exec_())