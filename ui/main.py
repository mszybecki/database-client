from PySide2.QtCore import Slot
from PySide2.QtWidgets import QHBoxLayout, QPushButton, QVBoxLayout, QWidget, QStackedWidget, QMainWindow
from ui import AddTableWidget, ViewTableWidget, DeleteTableWidget
from core import Event


class MainWidget(QWidget):
    def __init__(self, add_table_widget: AddTableWidget, view_table_widget: ViewTableWidget, delete_table_widget: DeleteTableWidget, event: Event):
        QWidget.__init__(self)
        self.__event = event

        self.add = QPushButton("Add table")
        self.view = QPushButton("View table")
        self.delete = QPushButton("Delete table")

        self.menu = QHBoxLayout()
        self.menu.addWidget(self.add)
        self.menu.addWidget(self.view)
        self.menu.addWidget(self.delete)

        self.content = QStackedWidget()
        self.content.addWidget(add_table_widget)
        self.content.addWidget(view_table_widget)
        self.content.addWidget(delete_table_widget)

        self.layout = QVBoxLayout()
        self.layout.addLayout(self.menu)
        self.layout.addWidget(self.content)

        self.setLayout(self.layout)

        self.add.clicked.connect(self.change_to_add_table_widget)
        self.view.clicked.connect(self.change_to_view_table_widget)
        self.delete.clicked.connect(self.change_to_delete_table_widget)

    @Slot()
    def change_to_add_table_widget(self):
        self.content.setCurrentIndex(0)

    @Slot()
    def change_to_view_table_widget(self):
        self.__event.emit("view-switch-to-first")
        self.content.setCurrentIndex(1)

    @Slot()
    def change_to_delete_table_widget(self):
        self.content.setCurrentIndex(2)


class MainWindow(QMainWindow):
    def __init__(self, widget: MainWidget, event: Event):
        QMainWindow.__init__(self)

        self.setWindowTitle("Database client")
        self.setCentralWidget(widget)
        self.setFixedHeight(720)
        self.setFixedWidth(1280)

        event.emit("reload-tables")

