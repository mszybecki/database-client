from PySide2.QtWidgets import QDialog, QPushButton, QLabel, QVBoxLayout, QHBoxLayout, QLineEdit


class Alert(QDialog):
    def __init__(self, text: str):
        super().__init__()
        self.closeBtn = QPushButton()
        self.closeBtn.setText("Close")
        self.closeBtn.clicked.connect(self.close)

        self.label = QLabel()
        self.label.setText(text)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.closeBtn)
        self.setLayout(self.layout)


class Prompt(QDialog):
    def __init__(self):
        super().__init__()
        self.acceptBtn = QPushButton()
        self.acceptBtn.setText("Accept")
        self.acceptBtn.clicked.connect(self.accept)

        self.closeBtn = QPushButton()
        self.closeBtn.setText("Close")
        self.closeBtn.clicked.connect(self.close)

        self.layout = QHBoxLayout()
        self.layout.addWidget(self.acceptBtn)
        self.layout.addWidget(self.closeBtn)
        self.setLayout(self.layout)

        self.__value = False

    def exec_(self) -> int:
        self.__value = False
        return super().exec_()

    def accept(self):
        self.__value = True
        self.close()

    def get_value(self) -> bool:
        return self.__value


class InputDialog(QDialog):
    def __init__(self, columns):
        super().__init__()

        self.acceptBtn = QPushButton()
        self.acceptBtn.setText("Accept")
        self.acceptBtn.clicked.connect(self.accept)

        self.closeBtn = QPushButton()
        self.closeBtn.setText("Close")
        self.closeBtn.clicked.connect(self.close)

        self.buttons = QHBoxLayout()
        self.buttons.addWidget(self.acceptBtn)
        self.buttons.addWidget(self.closeBtn)

        self.layout = QVBoxLayout()

        self.columns = {}
        for column in columns:
            if column != 'id':
                label = QLabel()
                label.setText(column)

                type = QLabel()
                type.setText(columns[column])

                input = QLineEdit()
                input.setPlaceholderText(column)

                layout = QHBoxLayout()
                layout.addWidget(label)
                layout.addWidget(type)
                layout.addWidget(input)

                self.layout.addLayout(layout)
                self.columns[column] = input

        self.layout.addLayout(self.buttons)
        self.setLayout(self.layout)

        self.__accepted = False

    def exec_(self) -> int:
        self.__accepted = False
        return super().exec_()

    def accept(self):
        self.__accepted = True
        self.close()

    def accepted(self) -> bool:
        return self.__accepted

    def get_value(self) -> dict:
        return {name: self.columns[name].text() for name in self.columns}


class TextInputDialog(QDialog):
    def __init__(self, label):
        super().__init__()

        self.label = QLabel()
        self.label.setText(label)

        self.input = QLineEdit()

        self.acceptBtn = QPushButton()
        self.acceptBtn.setText("Accept")
        self.acceptBtn.clicked.connect(self.accept)

        self.closeBtn = QPushButton()
        self.closeBtn.setText("Close")
        self.closeBtn.clicked.connect(self.close)

        self.buttons = QHBoxLayout()
        self.buttons.addWidget(self.acceptBtn)
        self.buttons.addWidget(self.closeBtn)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.input)
        self.layout.addLayout(self.buttons)

        self.setLayout(self.layout)

        self.__accepted = False

    def exec_(self) -> int:
        self.__accepted = False
        return super().exec_()

    def accept(self):
        self.__accepted = True
        self.close()

    def accepted(self) -> bool:
        return self.__accepted

    def get_value(self):
        return self.input.text()

