from PySide2.QtCore import Slot
from PySide2.QtWidgets import QLineEdit, QListWidget, QListWidgetItem, QHBoxLayout, QTableWidget, QTableWidgetItem, QLabel, QPushButton, QVBoxLayout, QWidget

from core import Database, Event
from ui import Prompt, Alert, InputDialog, TextInputDialog


class AddTableWidget(QWidget):
    def __init__(self, database: Database, event: Event):
        QWidget.__init__(self)

        self.__database = database
        self.__event = event

        self.label = QLabel("Write table name")
        self.tableName = QLineEdit()
        self.tableName.setPlaceholderText("Table name")

        self.addRowBtn = QPushButton()
        self.addRowBtn.setText("Add column")
        self.addRowBtn.clicked.connect(self.add_column)

        self.addTableBtn = QPushButton()
        self.addTableBtn.setText("Add table")
        self.addTableBtn.clicked.connect(self.add_table)

        self.columns = QVBoxLayout()
        self.columnsList = []

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.tableName)
        self.layout.addLayout(self.columns)
        self.layout.addWidget(self.addRowBtn)
        self.layout.addWidget(self.addTableBtn)

        self.setLayout(self.layout)

    @Slot()
    def add_table(self):
        table_name = self.tableName.text()

        columns = {}
        for column in self.columnsList:
            if column['name'].text() != "":
                columns[column['name'].text()] = column['type'].currentItem().text()
        try:
            self.__database.add_table(table_name, columns)
            self.__event.emit('reload-tables')

            self.columnsList = []
            for i in reversed(range(self.columns.count())):
                childLayout = self.columns.itemAt(i)

                for j in reversed(range(childLayout.count())):
                    childLayout.itemAt(j).widget().deleteLater()

                childLayout.deleteLater()

            self.tableName.setText("")

            alert = Alert('Sucessfully added table')
            alert.exec_()
            del alert

        except Exception as error:
            alert = Alert(error.__str__())
            alert.exec_()
            del alert

    @Slot()
    def add_column(self):
        label_column = QLabel()
        label_column.setText("Column name")

        line_edit = QLineEdit()
        line_edit.setPlaceholderText("Column name")

        label_type = QLabel()
        label_type.setText("Chose type of column")

        string_type = QListWidgetItem()
        string_type.setText("string")

        int_type = QListWidgetItem()
        int_type.setText("int")

        float_type = QListWidgetItem()
        float_type.setText("float")

        list_types = QListWidget()
        list_types.addItem(string_type)
        list_types.addItem(int_type)
        list_types.addItem(float_type)
        list_types.setCurrentRow(0)

        self.columnsList.append({
            "name": line_edit,
            "type": list_types
        })

        layout = QHBoxLayout()
        layout.addWidget(label_column)
        layout.addWidget(line_edit)
        layout.addWidget(label_type)
        layout.addWidget(list_types)

        self.columns.addLayout(layout)


class ViewTableWidget(QWidget):
    def __init__(self, database: Database, event: Event):
        QWidget.__init__(self)

        self.__database = database
        self.__event = event
        self.__event.subscribe("view-switch-to-first", self.switch_to_first)

        self.viewLayout = QVBoxLayout()

        self.setLayout(self.viewLayout)

        self.table_widget = None

    def switch_to_first(self):
        for i in reversed(range(self.viewLayout.count())):
            self.viewLayout.itemAt(i).widget().setParent(None)

        table_names = self.__database.get_tables_name()
        for table_name in table_names:
            widget = QPushButton()
            widget.setText(table_name)
            widget.clicked.connect(self.__bridge(self.switch_to_second, table_name))

            self.viewLayout.addWidget(widget)

    def switch_to_second(self, table_name):
        for i in reversed(range(self.viewLayout.count())):
            self.viewLayout.itemAt(i).widget().setParent(None)

        heads = self.__database.get_heads(table_name)
        rows = self.__database.get_rows(table_name)

        tableWidget = QTableWidget()
        tableWidget.setColumnCount(len(heads) + 1)
        tableWidget.setRowCount(len(rows))

        heads.update({'': ''})
        tableWidget.setHorizontalHeaderLabels(heads)

        for rowIndex in range(len(rows)):
            for i, j in enumerate(rows[rowIndex]):
                tableWidget.setItem(rowIndex, i, QTableWidgetItem(str(rows[rowIndex][j])))

            delete_row_btn = QPushButton()
            delete_row_btn.setText("delete this row")
            delete_row_btn.clicked.connect(self.__bridge(self.delete_row, table_name, rows[rowIndex]['id'], rowIndex))
            tableWidget.setCellWidget(rowIndex, len(heads) - 1, delete_row_btn)

        self.table_widget = tableWidget
        self.viewLayout.addWidget(tableWidget)

        add_row_button = QPushButton()
        add_row_button.setText("Add row")
        add_row_button.clicked.connect(self.__bridge(self.add_row, table_name))
        self.viewLayout.addWidget(add_row_button)

        search_button = QPushButton()
        search_button.setText("Search in table")
        search_button.clicked.connect(self.__bridge(self.search, table_name))
        self.viewLayout.addWidget(search_button)

    @Slot()
    def __bridge(self, func, *args):
        return lambda: func(*args)

    def add_row(self, table_name):
        dialog = InputDialog(self.__database.get_heads(table_name))
        dialog.exec_()

        if dialog.accepted():
            try:
                self.__database.add_row(table_name, dialog.get_value())
                self.switch_to_second(table_name)
            except Exception as error:
                alert = Alert(error.__str__())
                alert.exec_()
                del alert
        del dialog

    def delete_row(self, table_name, id, row):
        print(table_name)
        print(id)

        try:
            self.__database.remove_row(table_name, id)
            self.table_widget.removeRow(row)
        except Exception as error:
            alert = Alert(error.__str__())
            alert.exec_()
            del alert

    def search(self, table_name):
        text_input = TextInputDialog("Write you query")
        text_input.exec_()

        if text_input.accepted():
            filter = eval(text_input.get_value())
            rows = self.__database.search_in_table(table_name, filter)

            for i in reversed(range(self.viewLayout.count())):
                self.viewLayout.itemAt(i).widget().setParent(None)

            heads = self.__database.get_heads(table_name)

            tableWidget = QTableWidget()
            tableWidget.setColumnCount(len(heads) + 1)
            tableWidget.setRowCount(len(rows))

            heads.update({'': ''})
            tableWidget.setHorizontalHeaderLabels(heads)

            for rowIndex in range(len(rows)):
                for i, j in enumerate(rows[rowIndex]):
                    tableWidget.setItem(rowIndex, i, QTableWidgetItem(str(rows[rowIndex][j])))

                delete_row_btn = QPushButton()
                delete_row_btn.setText("delete this row")
                delete_row_btn.clicked.connect(
                    self.__bridge(self.delete_row, table_name, rows[rowIndex]['id'], rowIndex))
                tableWidget.setCellWidget(rowIndex, len(heads) - 1, delete_row_btn)

            self.table_widget = tableWidget
            self.viewLayout.addWidget(tableWidget)

        del text_input


class DeleteTableWidget(QWidget):
    def __init__(self, database: Database, event: Event, prompt: Prompt):
        QWidget.__init__(self)
        self.__database = database
        self.__prompt = prompt

        self.__event = event
        self.__event.subscribe("reload-tables", self.reload_tables)

        self.layout = QVBoxLayout()
        self.layout.addWidget(QLabel("To delete table click button below"))

        self.tableLayout = QVBoxLayout()
        self.layout.addLayout(self.tableLayout)

        self.setLayout(self.layout)

    def reload_tables(self):
        for i in reversed(range(self.tableLayout.count())):
            self.tableLayout.itemAt(i).widget().setParent(None)

        table_names = self.__database.get_tables_name()
        for table_name in table_names:
            widget = QPushButton()
            widget.setText(table_name)
            widget.clicked.connect(self.__bridge(table_name))

            self.tableLayout.addWidget(widget)

    def delete_table(self, table_name):
        self.__prompt.exec_()

        if self.__prompt.get_value():
            self.__database.remove_table(table_name)
            self.__event.emit("reload-tables")

    @Slot()
    def __bridge(self, table_name):
        return lambda: self.delete_table(table_name)
