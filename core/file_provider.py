import os

from core import Environment
from abc import ABC, abstractmethod


class FileProvider(ABC):
    @abstractmethod
    def create(self, filename: str):
        pass

    @abstractmethod
    def read(self, filename: str) -> list:
        pass

    @abstractmethod
    def save(self, filename: str, lines: list):
        pass

    @abstractmethod
    def remove(self, filename: str):
        pass

    @abstractmethod
    def tables(self):
        pass


class StandardFileProvider(FileProvider):
    def __init__(self, env: Environment):
        self.__env = env

    def create(self, filename: str):
        file = open(os.path.join(self.__get_base_dir(), filename), "w")
        file.close()

    def read(self, filename: str) -> list:
        file = open(os.path.join(self.__get_base_dir(), filename), "r")
        lines = []
        while True:
            line = file.readline()
            if line:
                lines.append(line.strip())
            else:
                break
        file.close()
        return lines

    def save(self, filename: str, lines: list):
        file = open(os.path.join(self.__get_base_dir(), filename), "w")
        for line in lines:
            file.write(line + '\n')
        file.close()
        return lines

    def remove(self, filename: str):
        os.remove(os.path.join(self.__get_base_dir(), filename))

    def __get_base_dir(self):
        return os.path.join(self.__env.get("base_dir"), "files")

    def tables(self):
        return [f for f in os.listdir(self.__get_base_dir()) if os.path.isfile(os.path.join(self.__get_base_dir(), f)) and f != ".gitignore"]


class BinaryFileProvider(FileProvider):
    def __init__(self, env: Environment):
        self.__env = env

    def create(self, filename: str):
        file = open(os.path.join(self.__get_base_dir(), filename), "wb")
        file.close()

    def remove(self, filename: str):
        os.remove(os.path.join(self.__get_base_dir(), filename))

    def read(self, filename: str):
        file = open(os.path.join(self.__get_base_dir(), filename), "rb")
        lines = []
        while True:
            line = file.readline().decode('utf-8')
            if line:
                lines.append(line.strip())
            else:
                break
        file.close()
        return lines

    def save(self, filename: str, lines: list):
        file = open(os.path.join(self.__get_base_dir(), filename), "wb")
        for line in lines:
            file.write((line + '\n').encode('utf-8'))
        file.close()
        return lines

    def __get_base_dir(self):
        return os.path.join(self.__env.get("base_dir"), "files")

    def tables(self):
        return [f for f in os.listdir(self.__get_base_dir()) if os.path.isfile(os.path.join(self.__get_base_dir(), f))]


