import unittest

from core import Environment, Event


class EnvTest(unittest.TestCase):

    def test_correctly_get_environment(self):
        env = Environment()

        env.put("env", "value")
        self.assertEqual(env.get("env"), "value")

    def test_that_double_key_cannot_exist(self):
        env = Environment()

        env.put("env", "value")
        self.assertRaises(Exception, lambda: env.put("env", "value2"))

    def test_that_key_not_exist(self):
        env = Environment()

        self.assertRaises(Exception, lambda: env.get("env"))


class EventTest(unittest.TestCase):
    def test_that_correctly_subscribed(self):
        self.x = 0

        event = Event()
        event.subscribe("test-method", self.method_for_test)
        event.emit("test-method")

        self.assertEqual(self.x, 1)

    def test_that_correctly_subscribed_to_two_methods(self):
        self.x = 0

        event = Event()
        event.subscribe("test-method", self.method_for_test)
        event.subscribe("test-method", self.method_for_test)
        event.emit("test-method")

        self.assertEqual(self.x, 2)

    def method_for_test(self):
        self.x += 1
