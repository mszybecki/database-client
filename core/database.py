from core import FileProvider, ServiceContainer


class Table:
    def __init__(self, file_provider: FileProvider, scope="prototype"):
        self.__file_provider = file_provider
        self.__table_name = ""
        self.__heads = {}
        self.__rows = []

    # create blank table
    def create(self, table_name, heads: dict):
        self.__table_name = table_name

        if "id" in heads.keys():
            raise Exception("Columns cannot contains id")

        self.__heads.update({'id': 'autoincrement'})
        self.__heads.update(heads)

    # add row and cast to target type
    def add_row(self, row: dict):
        if 'id' in row.keys():
            raise Exception("Row cannot contains id")

        new_row = {}
        for head in self.__heads:
            if head != 'id' and row[head] == '':
                raise Exception("Row cannot be empty")

            if self.__heads[head] == 'autoincrement':
                new_row[head] = 1 if len(self.__rows) == 0 else self.__rows[-1][head] + 1
            elif self.__heads[head] == 'int':
                new_row[head] = int(row[head])
            elif self.__heads[head] == 'float':
                new_row[head] = float(row[head])
            elif self.__heads[head] == 'string':
                new_row[head] = str(row[head])

        self.__rows.append(new_row)

    # remove row by id
    def remove_row(self, identifier: int):
        for row in self.__rows:
            if row["id"] == identifier:
                self.__rows.remove(row)
                return

    # get copy of rows
    def get_rows(self):
        return self.__rows.copy()

    def search(self, filter):
        tmp = []

        for row in self.__rows:
            if filter(row):
                tmp.append(row)

        return tmp

    # get copy of heads
    def get_heads(self):
        return self.__heads.copy()

    # read from file
    def read(self, table_name):
        self.__table_name = table_name
        lines = self.__file_provider.read(self.__table_name)

        for head in lines[0].split(";"):
            tmp = head.split(":")
            self.__heads[tmp[0]] = tmp[1]

        for row in lines[1:]:
            dict_row = {}
            for splitted in row.split(";"):
                tmp = splitted.split(":")
                if self.__heads[tmp[0]] == "autoincrement" or self.__heads[tmp[0]] == "int":
                    dict_row[tmp[0]] = int(tmp[1])
                elif self.__heads[tmp[0]] == "float":
                    dict_row[tmp[0]] = float(tmp[1])
                else:
                    dict_row[tmp[0]] = tmp[1]
            self.__rows.append(dict_row)

    # save to file
    def save(self):
        lines = []
        lines.append(self.__compact(self.__heads))
        for row in self.__rows:
            lines.append(self.__compact(row))
        self.__file_provider.create(self.__table_name)
        self.__file_provider.save(self.__table_name, lines)

    # remove table
    def remove(self):
        try:
            self.__file_provider.remove(self.__table_name)
        except:
            pass
        finally:
            del self.__rows
            del self.__heads
            del self.__table_name

    def __compact(self, dictionary: dict):
        tmp = ""
        for key in list(dictionary):
            tmp += key.replace(" ", "_")
            tmp += ":"
            tmp += str(dictionary.get(key))
            tmp += ";"
        tmp = tmp[:-1]
        return tmp


class Database:
    def __init__(self, container: ServiceContainer, file_provider: FileProvider):
        self.__container = container
        self.__file_provider = file_provider
        self.__tables = {}

    def get_tables_name(self):
        return self.__tables.keys()

    def add_row(self, table_name, row: dict):
        self.__tables[table_name].add_row(row)

    def remove_row(self, table_name, id: int):
        self.__tables[table_name].remove_row(id)

    def search_in_table(self, table_name, filter):
        return self.__tables[table_name].search(filter)

    def get_rows(self, table_name):
        return self.__tables[table_name].get_rows()

    def get_heads(self, table_name):
        return self.__tables[table_name].get_heads()

    def restore_tables(self):
        table_names = self.__file_provider.tables()

        for table_name in table_names:
            table = self.__container.get_bean(Table)
            table.read(table_name)
            self.__tables[table_name] = table

    def add_table(self, table_name: str, heads: dict):
        if table_name == "":
            raise Exception("Table name cannot be empty")

        if table_name in self.__tables.keys():
            raise Exception("That table name exists")

        if len(heads) == 0:
            raise Exception("Columns cannot be empty")

        table = self.__container.get_bean(Table)
        table.create(table_name, heads)
        self.__tables[table_name] = table

    def save_tables(self):
        for table_name in self.__tables:
            self.__tables[table_name].save()

    def remove_table(self, table_name):
        if table_name in self.__tables.keys():
            self.__tables[table_name].remove()
            del self.__tables[table_name]
