class Event:
    def __init__(self):
        self.__subscription = {}

    def subscribe(self, event_name, method):
        if not event_name in self.__subscription:
            self.__subscription[event_name] = []

        self.__subscription[event_name].append(method)

    def emit(self, event_name):
        for method in self.__subscription[event_name]:
            method()


class Environment:
    def __init__(self):
        self.__envs = []

    def put(self, key, value):
        if not self.__exists(key):
            self.__envs.append({"key": key, "value": value})
        else:
            raise Exception("Key: {} already exists".format(key))

    def get(self, key):
        for env in self.__envs:
            if env.get("key") == key:
                return env.get("value")
        raise Exception("There is not that key: {}".format(key))

    def __exists(self, key) -> bool:
        for env in self.__envs:
            if env.get("key") == key:
                return True
        return False
