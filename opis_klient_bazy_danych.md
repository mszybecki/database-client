##Opis zadania
* Program udający klienta bazy danych, pozwalający na utworzenie/usunięcie
tabeli, dodawanie/usuwanie wpisów w niej, oraz wyszukanie danych.
* Możliwe utworzenie tabeli
* dowolnej liczbie kolumn, każda kolumna zawierająca
dane typu liczba całkowitaalbo liczba rzeczywistaalbo tekst.
* Typ liczba całkowitapozwala na przechowywanie liczb całkowitych (pythonowy int).
* Typ liczba rzeczywista pozwala na przechowywanie liczb rzeczywistych (pythonowy
float).
* Typ tekstpozwala na przechowywanie dowolnego tekstu (domyślny typ tekstowy
pythona).
* Nazwy tabel i kolumn nie mogą być puste (muszą zawierać przynajmniej jeden
znak drukowalny).
* Główne okno powinno powinno posiadać listę tabel znajdujących się w bazie danych,
listę wpisów w zaznaczonej tabeli (lista lub dowolny czytelnysposób prezentacji
danych), oraz przyciski “Dodaj tabelę”, “Usuń tabelę”, “Dodaj wiersz”, “Usuń wiersz” i
“Wyszukaj w tabeli”.
* Okno dodawania tabeli powinno pozwalać na wpisanie nazwy tabeli oraz
dodanie kolumn (nazwa i typ).
* Okno dodawania wiersza powinno wyświetlać nazwy kolumn i pola tekstowe służące
wpisaniu danych. Walidacja typów wpisanych danych powinna odbywać się przy
próbie dodania wiersza (np. Jeśli pole tekstowe kolumny typu liczba całkowita
zawiera tekst nie rzutujący się na liczbę całkowitą, to powinien zostać wyświetlony
błąd - okienko, znacznik na polu tekstowym lub inne).
* Przed usunięciem tabeli/wiersza użytkownik powinien być proszony o potwierdzenie.
* Okno wyszukiwania powinno zawierać pole tekstowe, do którego wpisywany jest kod
lambda-wyrażenia zwracającego prawdę lub fałsz i przyjmującego jeden argument -
słownik kolumna:wartość. Lambda ta jest wywoływana dla każdego wiersza wybranej
tabeli, wyświetlane są wiersze dla których lambda zwróci prawdę. Przykładowo:
wpisanie “lambda row: row[“ID”]>5 or row[“ocena”]<3” zwróci wszystkie wiersze
których kolumna “ID” ma wartość większą od 5 lub kolumna “ocena” ma wartość
mniejszą od 3.
* Wynik wyszukiwania powinien być wyświetlony w oknie wyszukiwania (np. po utracie
focusa w polu tekstowym, po wciśnięciu przycisku wyszukiwania, po każdej edycji),
* Na wyższą ocenę: Po zamknięciu programu dane (tabele i ich zawartość)
powinny być zapisywane na dysk, a po jego uruchomieniu wczytywane.

##Testy
* Utworzenie tabeli “test1” z kolumnami liczbową “ID” (typ int), dwoma tekstowymi
“imię” oraz “nazwisko” oraz liczbową “wzrost” (typ float).
* Dodanie wiersza do tabeli “test1” z danymi “1”, “Roch”, “Przyłbipięt”, “1.50” -
oczekiwane powodzenie.
* Dodanie wiersza do tabeli “test1” z danymi “2”, “Ziemniaczysław”, “Bulwiasty”, “1.91” - oczekiwane powodzenie.
* Dodanie wiersza do tabeli “test1” z danymi “cztery”, “bla”, “bla”, “-90” - oczekiwane
niepowodzenie (dane tekstowe w polu liczbowym).
* Dodanie wiersza do tabeli “test1” z danymi “3.14”, “pi”, “ludolfina”, “314e-2” -
oczekiwane niepowodzenie (liczba rzeczywista w kolumnie z liczbę całkowitą).
* Wyświetlenie zawartości tabeli “test1”.
* Dodanie trzech kolejnych wierszy do tabeli “test1” i usunięcie dwóch wierszy z niej
(pierwszego i środkowego), w obu przypadkach najpierw anulowanie operacji, a
potem jej akceptacja.
* Utworzenie tabeli “test2” z kolumnami “reserved” typu stringoraz “kolor” typu liczba
całkowita.
* Dodanie wiersza do tabeli “test2” z danymi (puste pole), “1337” - oczekiwane
powodzenie.
* Dodanie wiersza do tabeli “test2” z danymi “bla”, “1939b” - oczekiwane
niepowodzenie (tekst w polu typu liczba całkowita).
* Usunięcie tabeli “test2”, najpierw anulowanie operacji, a potem jej akceptacja.
* Próba utworzenia tabeli bez nazwy - oczekiwane niepowodzenie.
* Próba utworzenia tabeli o nazwie “ “ (spacja) - oczekiwane niepowodzenie.
* Próba utworzenia tabeli z kolumną bez nazwy - oczekiwane niepowodzenie.
* Próba utworzenia tabeli z kolumną o nazwie “ “ (cztery spacje) - oczekiwane
niepowodzenie,
* Wypełnij tabelę “test1” danymi testowymi (kolejne wartości “ID, “wzrost” między 1.0 i 2.0)
, wyszukaj wiersze dla których “wzrost” ma wartość podaną przez prowadzącego oraz
“ID” jest liczbą parzystą lub nieparzystą (zależnie od woli prowadzącego).

##Publiczne repozytorium

https://gitlab.com/mszybecki/database-client